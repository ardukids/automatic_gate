#include <HCSR04.h>
#include <Servo.h>

#define TRIGGER 9
#define ECHO 8

UltraSonicDistanceSensor USDSensor(TRIGGER, ECHO);
Servo myServo;

void setup() 
{
  Serial.begin(115200); 
  myServo.attach(6);
}

void loop() 
{
  int distance = 0;
  distance = USDSensor.measureDistanceCm();

  //Serial.println(distance);

  if (distance <= 99)
  {
    myServo.write(90);
  }
  else
  {
    myServo.write(0);
  }

  delay(1000);
}
